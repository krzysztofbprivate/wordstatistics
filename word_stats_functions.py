# -*- coding: utf-8 -*-
"""
Created on Mon Mar 26 16:50:12 2018

@author: krzysztof.biegluk
"""

def dataCalculations(input_inputFile, input_limit):
    

    global sortedResultList
    global uniqueWordRatio
    global howManyWords
    global charLength
    global howManyUniqueWords
    global chunklist
    global x, y
    global limit
    global skladniki1, skladniki2, skladniki3
    
    #prepare data
    inputFile = open(input_inputFile,"r")
    wejscieOryg = inputFile.read()
    illegalChars = ["…","…",".",",",";","-","!","?",":","'",'"','“','”',"‘","’"]
    #exchange with regex:
    #oneOccurence = [m.start() for m in re.finditer(('[^a-zA-Z]' + lookUpWord + '[^a-zA-Z]'), skladnik)]
    
    for illegalChar in illegalChars:
            wejscie = wejscieOryg.replace(illegalChar, '')
    wejscie = wejscie.lower()
    wordList = wejscie.split()
    inputFile.close()
    
    indexList = []
    indexListCount = []      
    for word in wordList:
        if not word in indexList:
            indexList.append(word)
            indexListCount.insert(indexList.index(word), 1)
        else:
            indexListCount[indexList.index(word)] += 1
    
    jointIndexList = []
    i = 0
    for element in indexList:
        jointIndexList.append([indexListCount[i],indexList[i]])
        i += 1
    
   
    
    #stat calculations
    howManyUniqueWords = len(indexList)
    charLength = len(wejscieOryg)
    howManyWords = len(wordList)
    sortedResultList = sorted(jointIndexList, key=lambda tup: tup[0], reverse=True)
    uniqueWordRatio = round((float(howManyUniqueWords) / float(howManyWords))*100, 2)
    
    #create chart variables list
    try:
        limit = input_limit
    except:
        limit = 20
    x = []
    y = []
    for elem in sortedResultList:
        x.append(elem[1])
        y.append(elem[0])
        if len(x)>=limit:
            break


    #create context fragments
    import re
    import random
    
    lookUpWord = sortedResultList[round(random.random()*30)][1]
    chunklist = []    
    listOfOccurences = [m.start() for m in re.finditer(('[^a-zA-Z]' + lookUpWord + '[^a-zA-Z]'), wejscieOryg)]
    for occurence in listOfOccurences:
        if occurence > 100:
            try:
                chunklist.append(wejscieOryg[occurence - 100 : occurence + 100]) 
            except:
                chunklist.append(wejscieOryg[occurence - 100 : occurence + len(wejscieOryg) - occurence - 1])
        else:
            try:
                chunklist.append(wejscieOryg[0: occurence + 100]) 
            except:
                chunklist.append(wejscieOryg[0: occurence + len(wejscieOryg) - occurence - 1])
                
    #cut fragments to contain full words only
    chunklistCounter = 0
    for chunk in chunklist:
        chunk = chunk[chunk.find(' '):chunk.rfind(' ')]
        chunklist[chunklistCounter] = chunk    
        chunklistCounter += 1
    
    #split fragments to whats before and after lookUpWord and list separately
    skladniki1 = []
    skladniki2 = []
    skladniki3 = []
    for skladnik in chunklist:
        oneOccurence = [m.start() for m in re.finditer(('[^a-zA-Z]' + lookUpWord + '[^a-zA-Z]'), skladnik)]
        skladniki1.append(skladnik[0: oneOccurence[0]+1])
        skladniki2.append(lookUpWord)
        skladniki3.append(skladnik[oneOccurence[0] + len(lookUpWord)+1:len(skladnik)])
     
        



def createReport(output_outputFile):
    
    '''
    global sortedResultList
    global uniqueWordRatio
    global howManyWords
    global charLength
    global howManyUniqueWords
    '''


    with open(output_outputFile, "w") as output_file:
    
        #import css style
        output_file.write('<html><head><style>\n')
        with open("sources/css.txt", "r") as css_file:
            cssStyle = css_file.read()
            output_file.write(cssStyle)
        css_file.close()
        output_file.write('\n</style>')
               
        #import first part chart generation script definition
        with open("sources/script.txt", "r") as scriptFile:
            scriptjs = scriptFile.read()
            output_file.write(scriptjs)
        scriptFile.close()
        
        #generate chart variables
        ij = 0
        for element in x:
            if ij >= limit:
                break
            output_file.write("['" + str(x[ij]) + "'," + str(y[ij]) + "],\n")
            ij+=1
    
        #import last part of chart generation script
        with open("sources/script2.txt", "r") as scriptFile:
            scriptjs = scriptFile.read()
            output_file.write(scriptjs)
        scriptFile.close()
        
        #start writing html content
        output_file.write('</head>')
        output_file.write('<body>')  
        output_file.write('<div class="container-fluid" width="800px">')
        output_file.write('<div class="row"><div class="column">')
    
        #write top words chart
        output_file.write('<p><fieldset style="width:500px; height:400px"><legend>Top ' + str(limit) + ' words</legend>')
        output_file.write('<div id="chart_div"></div>')
        output_file.write('</fieldset>')
        #write top words chart - end
        
        #write often occuring words
        output_file.write('<p><fieldset style="width:500px"><legend>Words used more than once on an A4 page (on average)</legend>')
        output_file.write('<table class = "t01" style="height: 500px">')
        output_file.write('<tr><th>word</th><th>count</th></tr>')
        for element in sortedResultList:
            if element[0] > howManyWords / 375:
                output_file.write('<tr><td>' + str(element[1]) + '</td><td>' + str(element[0]) + '</td></tr>')
        output_file.write('</table></fieldset></div></div>')
        output_file.write('<div class="row"><div class="column">')
        #write often occuring words - end
        
        #basic stats
        output_file.write('<p><fieldset style="width:500px"><legend>Basic stats</legend>')
        output_file.write('<table id = "t01">')
        output_file.write('<tr><td>Total word count: </td><td>' + str(howManyWords) + '</td></tr>')
        output_file.write('<tr><td>Total char count: </td><td>' + str(charLength) + '</td></tr>')
        output_file.write('<tr><td>Unique word count: </td><td>' + str(howManyUniqueWords) + '</td></tr>')
        output_file.write('<tr><td>Unique word ratio: </td><td>' + str(uniqueWordRatio) + '%</td></tr>') 
        output_file.write('</table></fieldset>')
        #basic stats
        
        #write occurences
        output_file.write('<p><fieldset style="heigth:1000px"><legend>Occurences</legend>')
        output_file.write('<table>')
        #output_file.write('<table id="t01">')
        klm = 0
        for chunk in chunklist:
            output_file.write('<tr><td>(...) ' + skladniki1[klm] + '<b>' + skladniki2[klm] + '</b>' + skladniki3[klm] + ' (...)</td></tr>')
            klm += 1
            if klm > 15:
                break
        output_file.write('</table></fieldset>')
    
        #finished writing html content
        output_file.write('</div></div></body></html>')
    output_file.close()

def openReport(input_reportFile):
    
    #open report 
    import webbrowser
    import os
    try:
        from urllib import pathname2url         # Python 2.x
    except:
        from urllib.request import pathname2url # Python 3.x
        
    chrome_path = "C:/Program Files (x86)/Google/Chrome/Application/chrome.exe %s"
    url = 'file:{}'.format(pathname2url(os.path.abspath(input_reportFile)))
    try:
        webbrowser.get(chrome_path).open(url)
    except:
        webbrowser.open(url)

inputLimit = 21
inputFile = "D:/Users/krzysztof.biegluk/Desktop/python/word_stats/sources/input.txt"
reportFile = "D:/Users/krzysztof.biegluk/Desktop/python/word_stats/output.html"
dataCalculations(inputFile,inputLimit)
createReport(reportFile)
openReport(reportFile)

