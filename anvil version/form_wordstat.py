from anvil import *
import tables
from tables import app_tables
import anvil.server


class form_wordstat (form_wordstatTemplate):
  def __init__(self, **properties):
    # You must call self.init_components() before doing anything else in this function
    self.init_components(**properties)
    # Any code you write here will run when the form opens.


  def button_1_click (self, **event_args):
    self.label_wordstat.visible = True
    self.button_wordstat.visible = True
    self.text_area_wordstat.visible = True
    self.text_box_limitnumber.visible = True
    self.label_limit.visible = True
    self.label_limit_descr.visible = True
    self.label_expl.visible = False
    self.button_example.visible = True
    self.label_example_descr.visible = True
    self.xy_panel_2.visible = True
    

  def button_wordstat_click (self, **event_args):
    global reporthtml
    from tables import app_tables
    
    self.label_error.visible = False
    if len(self.text_area_wordstat.text) < 400:
      self.label_error.visible = True
      self.label_error.text = "Please load longer text - let's make it sweat!"
      
    else:     
      #read and store input
      textFromForm = self.text_area_wordstat.text
      limitFromForm = int(self.text_box_limitnumber.text)
      app_tables.inputtable.delete_all_rows()
      app_tables.inputtable.add_row(Type = "limit")
      app_tables.inputtable.add_row(Type = "inputString")
      inputTableRow = app_tables.inputtable.get(Type = "limit")
      inputTableRow.update(Value = str(limitFromForm))
      inputTableRow = app_tables.inputtable.get(Type = "inputString")
      inputTableRow.update(Value = textFromForm)
      
      open_form('form_wordstat_webview')    
 
  def button_example_click (self, **event_args):
    rowExample = app_tables.sourcetable.get(name="identifier")
    self.text_area_wordstat.text = rowExample['example']

  def button_testing_click (self, **event_args):
    open_form('form_wordstat_webview')
    pass

