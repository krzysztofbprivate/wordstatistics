# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import tables
from tables import app_tables
import anvil.server
import re

@anvil.server.callable
def performCalculations():
    
    wejscieOryg = app_tables.inputtable.get(Type = 'inputString')['Value']  
    illegalChars = """.,;:-!?'[]()\/""" + '''"'''
    
    for illegalChar in illegalChars:
        wejscie = wejscieOryg.replace(illegalChar, '')
    
    wejscie = wejscie.lower()
    wordList = wejscie.split()
    
    indexList = []
    indexListCount = []      
    for word in wordList:
        if not word in indexList:
            indexList.append(word)
            indexListCount.insert(indexList.index(word), 1)
        else:
            indexListCount[indexList.index(word)] += 1
    
    jointIndexList = []
    i = 0
    for element in indexList:
        jointIndexList.append([indexListCount[i],indexList[i]])
        i += 1
    
    #basic stat calculations
    howManyUniqueWords = len(indexList)
    charLength = len(wejscieOryg)
    howManyWords = len(wordList)
    sortedResultList = sorted(jointIndexList, key=lambda tup: tup[0], reverse=True)
    uniqueWordRatio = round((float(howManyUniqueWords) / float(howManyWords))*100, 2)
    
    app_tables.outputtablestats.delete_all_rows()
    app_tables.outputtablestats.add_row(Type = "Words", Value = str(howManyWords))
    app_tables.outputtablestats.add_row(Type = "Characters", Value = str(charLength))
    app_tables.outputtablestats.add_row(Type = "Unique words", Value = str(howManyUniqueWords))
    app_tables.outputtablestats.add_row(Type = "Unique word ratio", Value = (str(uniqueWordRatio) + '%'))
    
    return sortedResultList

@anvil.server.callable    
def prepareWriteContextFragments(lookUpWord):
    
    #create context fragments    
    import random
    
    wejscieOryg = app_tables.inputtable.get(Type = 'inputString')['Value'] #if this does not work, use two lines below
    #wejscieOrygRow = app_tables.inputtable.get(Type = 'inputString')
    #wejscieOryg = wejscieOrygRow['Value']
    chunklist = []    
    listOfOccurences = [m.start() for m in re.finditer(('[^a-zA-Z]' + lookUpWord + '[^a-zA-Z]'), wejscieOryg)]
    for occurence in listOfOccurences:
        if occurence > 100:
            try:
                chunklist.append(wejscieOryg[occurence - 100 : occurence + 100]) 
            except:
                chunklist.append(wejscieOryg[occurence - 100 : occurence + len(wejscieOryg) - occurence - 1])
        else:
            try:
                chunklist.append(wejscieOryg[0: occurence + 100]) 
            except:
                chunklist.append(wejscieOryg[0: occurence + len(wejscieOryg) - occurence - 1])
     
    #cut fragments to contain full words only
    chunklistCounter = 0
    for chunk in chunklist:
        chunk = chunk[chunk.find(' '):chunk.rfind(' ')]
        chunklist[chunklistCounter] = chunk    
        chunklistCounter += 1
   
    #split fragments to whats before and after lookUpWord and list separately
    skladniki1 = []
    skladniki3 = []
    for skladnik in chunklist:
        oneOccurence = [m.start() for m in re.finditer(('[^a-zA-Z]' + lookUpWord + '[^a-zA-Z]'), skladnik)]
        skladniki1.append(skladnik[0: oneOccurence[0]+1])
        skladniki3.append(skladnik[oneOccurence[0] + len(lookUpWord)+1:len(skladnik)])
    
    app_tables.chunktable.delete_all_rows()
    klm = 0
    while klm < len (skladniki1):  
      app_tables.chunktable.add_row(chunkElementOne = skladniki1[klm], chunkElementTwo = lookUpWord, chunkElementThree = skladniki3[klm], fullChunk = "(...)" + skladniki1[klm] + lookUpWord + skladniki3[klm] + " (...)")
      klm += 1
      
      

@anvil.server.callable
def chartVariablesDefinition():
    
    #create chart variables
    app_tables.charttable.delete_all_rows()
    limit = int(app_tables.inputtable.get(Type = 'limit')['Value'])
    ij = 0
    for row in app_tables.outputtablelist.search():
      if ij >= limit:
        break
      app_tables.charttable.add_row(chartWord = row["sortedResultListText"], chartNumber = row["sortedResultListNumber"])
      ij += 1